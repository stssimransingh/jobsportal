<div class="row">
    <div class="col-md-12 text-white text-center bg" style="background-image:url('<?php echo base_url('assets/img/slider-bg.jpg'); ?>');">
        <h2 style="margin-top:100px;">Find your desired job in a minute</h2>
        <h1>Join us & Explore Thousands of Jobs</h1>
        <div class="row mb-5 mt-5">
            <div class=" col-md-10 mx-auto mb-5" style="display-inline;">
                <div class="border mx-auto">
                    <div>
                        <input
                            type="text"
                            class="p-3"
                            placeholder="job title,skills or company"
                            style="background-color:white;color:black;width:62%"/>
                        <input
                            type="text"
                            class="p-3"
                            placeholder="Category..."
                            style="background-color:white;color:black"/>
                        <input
                            type="button"
                            class="btn-info btn-lg text-white p-3"
                            value="SEARCH"
                            style="height:65px;width:150px;"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- 2 row end -->
<!-- 3 row end -->

<div class="row">
    <div class="col-md-12 text-center" style="margin-top:80px;">
        <h1>Browse<span style="color:skyblue">
                Jobs</h1>
        </span>
    </div>
</div>
<!-- 4 row end -->

<div class="mx-5 my-5">
    <div class="row text-center ml-5 mr-5">
        <div class="col-md-3 icon p-5">
            <a href="">
                <i
                    class="fa fa-subway"
                    aria-hidden="true"
                    style="height:50px;width:50px; color:red"></i>
            </a>
            <h4>
                <p>Railway</p>
            </h4>
        </div>
        <div class="col-md-3 icon p-5">
            <a href="">
                <img src="tank2.png" style="height:50px;width:50px;color:skyblue;"></a>
                <h4>Indian Army</h4>
            </div>
            <div class="col-md-3 icon p-5">
                <a href="">
                    <img src="police.png" style="height:50px;width:50px;color:red;"></a>
                    <h4>
                        <p>Police</p>
                    </h4>
                </div>
                <div class="col-md-3 icon p-5 ">
                    <a href="">
                        <i class="fas fa-fighter-jet" style="height:50px;width:50px;color:skyblue;"></i>
                    </a>
                    <h4>Air Force</h4>
                </div>
            </div>
            <!-- 5th row end -->

            <div class="row mt-3 text-center ml-5 mr-5 mt-5">
                <div class="col-md-3 icon p-5">
                    <a href="">
                        <i class="fas fa-ship" style="height:50;width:50px;color:skyblue;"></i>
                    </a>
                    <h4>Navy</h4>
                </div>
                <div class="col-md-3 icon p-5">
                    <a href="">
                        <i
                            class="fa fa-gavel"
                            aria-hidden="true"
                            style="height:50px;width:50px;color:red"></i>
                    </a>
                    <h4>
                        <p>Legal Job</p>
                    </h4>
                </div>
                <div class="col-md-3 icon p-5">
                    <a href=""><img src="bank2.png" style="height:50px;width:50px;"/></a>
                    <h4>Banking</h4>
                </div>
                <div class="col-md-3 icon p-5">
                    <a href="">
                        <i
                            class="fas fa-university"
                            aria-hidden="true"
                            style="height:50px;width:50px; color:red;"></i>
                    </a>
                    <h4>
                        <p>Education</p>
                    </h4>
                </div>
            </div>
            <!-- 6th row end -->

            <div class="text-center">
                <button class="btn btn-lg btn-danger text-white mt-5">VIEW ALL CATEGORIES</button>
            </div>
        </div>
        <!-- div end -->

        <div class="row mt-5 bg-light p-5">
            <div class="col-md-12  text-center mb-5">

                <img src="map.png">
                    <h1>Featured<span style="color:skyblue">
                            Jobs</h1>
                    </span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut et viverra nulla.
                        Fusce at rhoncus diam, quis convallis ligula<br/>
                        Cras et ligula aliquet, ultrices leo non, scelerisque justo. Nunc a vehicula
                        augue.</p>
                </div>
                <div class="row mx-auto">
                    <div class="col-md-4 bg-white p-5">
                        <img src="education.png">
                            <div class="">
                                <strong>Education Department</strong>
                                <p>Get your job</p>
                                <a href="" class=" btn btn-danger text-white">Get detail</a>
                                <a href="" class="btn btn-info text-white">Apply now</a>
                            </div>
                        </div>
                        <div class="col-md-4 bg-white p-5">
                            <img src="defence.png">
                                <div class="">
                                    <strong>Defence Department</strong>
                                    <p>Get your job</p>
                                    <a href="" class=" btn btn-danger text-white">Get detail</a>
                                    <a href="" class="btn btn-info text-white">Apply now</a>
                                </div>
                            </div>
                            <div class="col-md-4 bg-white p-5">
                                <img src="law-justice.png">
                                    <div class="">
                                        <strong>Legal Department</strong>
                                        <p>Get your job</p>
                                        <a href="" class=" btn btn-danger text-white">Get detail</a>
                                        <a href="" class="btn btn-info text-white">Apply now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- 7th and 8th row end -->

                        <div class="row">
                            <div class="col-md-12  text-white text-center bg1" style="padding:8%;">
                                <h1>All Govt.Recruitments</h1>
                                <p>Instead of being unsatisfied in your job, find a role that you love going to
                                    each day! Find fulfillment by doing meaningful work for your community.Now is a
                                    great time to pursue a career in the public sector, because there are not enough
                                    applicants to fill the open jobs. Take advantage of this, apply for a government
                                    job today!</p>
                                <br/>
                                <a href="" class="btn btn-danger btn-lg mt-2">Read More</a>
                            </p>

                        </div>
                    </div>
                    <!-- 9th row end -->
                    <div class="row bg-dark">
                        <div class="col-md-9 text-white text-center mt-5 mx-auto">
                            <h5 >Copyright © 2019 All Govt.Recruitments - All Rights Reserved.</h5>
                        </div>
                        <div class="col-md-3 text-white mt-5 mx-auto text-center">
                            <h6>Follow us on</h6>
                            <a href="https://www.facebook.com/" class="custm">
                                <div
                                    class=" box text-center"
                                    style="width:30px; height:30px;line-height:30px;background-color:white;border-radius:50%;">
                                    <i class="fab fa-facebook-f" style="color:black;"></i>
                                </div>
                            </a>
                            <a href="https://www.google.com/" class="custm">
                                <div
                                    class=" box text-center"
                                    style="width:30px; height:30px;line-height:30px;background-color:white; border-radius:50%;">
                                    <i class="fab fa-google text-black"></i>
                                </div>
                            </a>
                            <a href="https://twitter.com/">
                                <div
                                    class=" box text-center"
                                    style="width:30px; height:30px;line-height:30px;background-color:white;border-radius:50%;">
                                    <i class="fab fa-twitter text-black"></i>
                                </div>
                            </a>
                            <a href="https://www.instagram.com/" class="custm">
                                <div
                                    class=" box text-center"
                                    style="width:30px; height:30px;line-height:30px;background-color:white;border-radius:50%;">
                                    <i class="fab fa-instagram text-black"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>