<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://fonts.googleapis.com/css?family=Alice&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/stylestyle.css'); ?>">
    <script src="<?php echo base_url('assets/jquery.js'); ?>">
    </script>
    <script src="<?php echo base_url('assets/js/bootstrap.js'); ?>">
    </script>
    <script src="<?php echo base_url('assets/fontawesome/js/all.js'); ?>"></script>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 menu text-black bg-white">
            <nav class="navbar navbar-light navbar-expand-md">
                <span class="navbar-brand" style="margin-left:100px;">
                    <img src="flogo.png"/></span>
                <strong>
                    <ul class="navbar-nav" style="margin-left:120px;">
                        <li class="nav-item mt-2">
                            <a href="index1.php" class="active nav-link">Home</a>
                        </li>
                        <li class="nav-item mt-2 ">
                            <a href="about.php" class="nav-link">About us</a>
                        </li>
                        <li class="nav-item  mt-2 ">
                            <a href="contact.php" class="nav-link">Contact us</a>
                        </li>
                        <button class="btn btn-danger ml-3" data-toggle="modal" data-target="#mymodal">Leave a Comment</button>
                        <div class="modal" id="mymodal">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header text-info">Please let us know your experience with our website</div>
                                    <div class="modal-body">
                                        <label for="">Email</label>
                                        <input
                                            type="email"
                                            class="form-control"
                                            placeholder="enter your mail"
                                            name="email"/>
                                        <label for="" class="mt-3">Feedback</label>
                                        <textarea
                                            rows="4"
                                            cols="54"
                                            class="form-control"
                                            placeholder="Type Your Message here..."
                                            name="message"></textarea>
                                        <button class="btn btn-danger mt-3">Submit</button>
                                    </div>
                                    <div class="modal-footer">
                                        <button data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ul>
                </strong>
                <div>
                    <a href="login.php" Class=" ml-5 abcd">
                        <i class="fas fa-lock" style="color:red;"></i>&nbsp;&nbsp; Login</a>
                    <a href="register.php" class="ml-5 abcd">
                        <i class="fas fa-user" style="color:red;"></i>&nbsp;&nbsp; Register</a>
                </div>
            </nav>
        </div>
    </div>
          <!-- 1 row end -->